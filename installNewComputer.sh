# Instalations of Packages
sudo apt-get install git ssh
sudo apt-get install xournal xfig okular 
sudo apt-get install gufw kgpg 
sudo apt-get install vlc audacity lmms 
sudo apt-get install texlive-latex-base texlive-latex-extra latex-beamer latexdiff

# Installations from Sources
mkdir ~/INSTALL
cd ~/INSTALL/

# INSTALL Latest version of Emacs
sudo apt-get install build-essential shtool
sudo apt-get build-dep emacs24
git clone --depth 1 git://git.sv.gnu.org/emacs.git
cd emacs/
./autogen.sh 
./configure 
make
sudo make install
cd -
